#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include "rs232.h"

int rs232_open(char* port, int baud);
void rs232_close(int fd);
int rs232_read(int fd, void* buf, size_t size);
int rs232_write(int fd, void* buf, size_t size);

#define SERIAL_PORT "/dev/tty.usbserial"
#define SERIAL_BAUD (9600)
#define BAUD_DIVISOR (5000000000)

int fprom_send_u8(int fd, uint8_t data)
{
  uint8_t d8[1];
  d8[0] = data;
  fflush(stdout);
  rs232_write_block(fd, d8, 1);
  usleep(BAUD_DIVISOR/SERIAL_BAUD);
  return 1;
}

int fprom_send_u16(int fd, uint16_t data)
{
  uint8_t d8[2];
  d8[0] = data >> 0;
  d8[1] = data >> 8;
  if(!fprom_send_u8(fd, d8[0]))
    return 0;
  if(!fprom_send_u8(fd, d8[1]))
    return 0;
  return 1;
}

int fprom_send_u32(int fd, uint32_t data)
{
  uint16_t d16[2];
  d16[0] = data >> 0;
  d16[1] = data >> 16;
  if(!fprom_send_u16(fd, d16[0]))
    return 0;
  if(!fprom_send_u16(fd, d16[1]))
    return 0;
  return 1;
}

int fprom_send_lead_in(int fd)
{
  if(!fprom_send_u16(fd, 0xCA53))
    return 0;
  return 1;
}

int fprom_send_lead_out(int fd)
{
  if(!fprom_send_u16(fd, 0x35AC))
    return 0;
  return 1;
}

int fprom_send_rom(int fd, void* buf, size_t size)
{
  int ret = 0;
  size_t words, i;
  uint8_t* data = buf;

  words = size;

  for(i = 0; i < words; i++) {
    printf(" Uploading... 0x%lX/0x%lX bytes (%lu%%)\x1B[1G", i+1, words, ((i+1) * 100) / words);
    if(!fprom_send_u8(fd, data[i])) {
      perror("Send byte failure");
      goto _fail;
    }
  }

  printf("Done! Letting UART catch up...\n");
  sleep(1);

  ret = 1;
_fail:
  return ret;
}

int main(int argc, char *argv[])
{
  int ret = EXIT_FAILURE;
  int fd;
  FILE* fp;
  void* buf;
  size_t size;

  fd = rs232_open(SERIAL_PORT, SERIAL_BAUD);
  if(fd < 0) {
    perror("RS232 open failure");
    return EXIT_FAILURE;
  }

  fp = fopen(argv[1], "rb");
  if(fp == NULL) {
    perror("Open input file failure");
    goto _fail;
  }
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  buf = malloc(size);
  if(buf == NULL) {
    perror("Alloc file buffer failure");
    goto _fail;
  }
  fread(buf, size, 1, fp);
  fclose(fp);

  if(!fprom_send_rom(fd, buf, size)) {
    perror("Send ROM failure");
    goto _fail;
  }

  ret = EXIT_SUCCESS;
_fail:
  rs232_close(fd);
  return ret;
}

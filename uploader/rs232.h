#ifndef RS232_H_
#define RS232_H_

int rs232_open(char* port, int baud);
void rs232_close(int fd);
int rs232_read(int fd, void* buf, size_t size);
int rs232_write(int fd, void* buf, size_t size);
int rs232_read_block(int fd, void* buf, size_t size);
int rs232_write_block(int fd, void* buf, size_t size);

#endif

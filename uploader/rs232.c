#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>

#include "rs232.h"

struct termios old_tios, new_tios;

int rs232_open(char* port, int baud)
{
  int fd, baudr, status;

  switch(baud) {
    case      50: baudr = B50; break;
    case      75: baudr = B75; break;
    case     110: baudr = B110; break;
    case     134: baudr = B134; break;
    case     150: baudr = B150; break;
    case     200: baudr = B200; break;
    case     300: baudr = B300; break;
    case     600: baudr = B600; break;
    case    1200: baudr = B1200; break;
    case    1800: baudr = B1800; break;
    case    2400: baudr = B2400; break;
    case    4800: baudr = B4800; break;
    case    9600: baudr = B9600; break;
    case   19200: baudr = B19200; break;
    case   38400: baudr = B38400; break;
    case   57600: baudr = B57600; break;
    case  115200: baudr = B115200; break;
    case  230400: baudr = B230400; break;
    default:
      return -1;
  }

  fd = open(port, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if(fd < 0) {
    perror("port open failure");
    return -1;
  }

  if(tcgetattr(fd, &old_tios) < 0) {
    close(fd);
    perror("port tcgetattr failure");
    return -1;
  }
  new_tios = old_tios;

  new_tios.c_cflag = CS8 | CLOCAL | CREAD;
  new_tios.c_iflag = IGNPAR;
  new_tios.c_oflag = 0;
  new_tios.c_lflag = 0;
  new_tios.c_cc[VMIN] = 1;
  new_tios.c_cc[VTIME] = 0;
  cfsetispeed(&new_tios, baudr);
  cfsetospeed(&new_tios, baudr);
  if(tcsetattr(fd, TCSAFLUSH, &new_tios) < 0) {
    close(fd);
    perror("port tcsetattr failure");
    return -1;
  }

  if(ioctl(fd, TIOCMGET, &status) < 0) {
    perror("TIOCMGET failure");
    return -1;
  }

  status &= ~TIOCM_DTR;    /* turn on DTR */
  status &= ~TIOCM_RTS;    /* turn on RTS */

  if(ioctl(fd, TIOCMSET, &status) < 0) {
    perror("TIOCMSET failure");
    return -1;
  }

  return fd;
}

void rs232_close(int fd)
{
  int status;

  if(ioctl(fd, TIOCMGET, &status) < 0) {
    perror("TIOCMGET failure");
  }

  status &= ~TIOCM_DTR;    /* turn off DTR */
  status &= ~TIOCM_RTS;    /* turn off RTS */

  if(ioctl(fd, TIOCMSET, &status) < 0) {
    perror("TIOCMSET failure");
  }

  tcsetattr(fd, TCSANOW, &old_tios);
  close(fd);
}

int rs232_read(int fd, void* buf, size_t size)
{
  return read(fd, buf, size);
}

int rs232_write(int fd, void* buf, size_t size)
{
  return write(fd, buf, size);
}

int rs232_read_block(int fd, void* buf, size_t size)
{
  int ret;
  do {
    ret = read(fd, buf, size);
  } while(ret == -1 && errno == EAGAIN);
  return ret;
}

int rs232_write_block(int fd, void* buf, size_t size)
{
  int ret;
  do {
    ret = write(fd, buf, size);
  } while(ret == -1 && errno == EAGAIN);
  return ret;
}

